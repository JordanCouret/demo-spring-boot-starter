package fr.jco.cpedemo.step2;

import fr.jco.demo.starterrandombeans.RandomGeneratorBean;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/step2")
public class ControllerUsingAutoConfigureBeans {

	private final RandomGeneratorBean randomGeneratorBean;

	public ControllerUsingAutoConfigureBeans(RandomGeneratorBean randomGeneratorBean) {
		this.randomGeneratorBean = randomGeneratorBean;
	}

	@GetMapping
	public int get() {
		return randomGeneratorBean.getInt();
	}

}
