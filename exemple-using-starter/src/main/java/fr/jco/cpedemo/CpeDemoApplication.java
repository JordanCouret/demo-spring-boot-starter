package fr.jco.cpedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackages = "fr.jco.cpedemo.step1")
@ComponentScan(basePackages = "fr.jco.cpedemo.step2")
public class CpeDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CpeDemoApplication.class, args);
	}

}
