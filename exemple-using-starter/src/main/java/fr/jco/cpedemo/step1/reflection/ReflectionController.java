package fr.jco.cpedemo.step1.reflection;


import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/step1/reflection")
public class ReflectionController {

	private final SomeBeanWithReflection someBeanWithReflection;

	private final AtomicLong increment = new AtomicLong(0);

	public ReflectionController(SomeBeanWithReflection someBeanWithReflection) {
		this.someBeanWithReflection = someBeanWithReflection;
	}

	@GetMapping
	private Long get() throws IllegalAccessException {
		final Field properties = ReflectionUtils.findField(SomeBeanWithReflection.class, "properties");
		if (properties != null) {
			ReflectionUtils.makeAccessible(properties);
			properties.set(this.someBeanWithReflection, increment.getAndIncrement());
		}
		return this.someBeanWithReflection.getProperties();
	}

}
