package fr.jco.cpedemo.step1.reflection;

import org.springframework.stereotype.Service;

@Service
public class SomeBeanWithReflection {

	private Long properties;

	public SomeBeanWithReflection() {

	}

	public Long getProperties() {
		return properties;
	}
}
