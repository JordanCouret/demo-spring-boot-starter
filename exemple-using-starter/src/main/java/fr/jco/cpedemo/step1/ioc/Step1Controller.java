package fr.jco.cpedemo.step1.ioc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/step1/ioc")
public class Step1Controller {
	private final Logger log = LoggerFactory.getLogger(Step1Controller.class);

	private final BeanInjectionWithSetter beanInjectionWithSetter;

	private final BeanInjectionWithConstructor beanInjectionWithConstructor;

	public Step1Controller(BeanInjectionWithSetter beanInjectionWithSetter, BeanInjectionWithConstructor beanInjectionWithConstructor) {
		log.info("[{}] Dépendances injectées via la constructeur", this.getClass().getSimpleName());
		this.beanInjectionWithSetter = beanInjectionWithSetter;
		this.beanInjectionWithConstructor = beanInjectionWithConstructor;
	}

	@GetMapping
	public void get() {
		log.info("Get Call");
	}


}
