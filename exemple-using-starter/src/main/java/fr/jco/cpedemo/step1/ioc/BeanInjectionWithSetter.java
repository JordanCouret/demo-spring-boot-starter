package fr.jco.cpedemo.step1.ioc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class BeanInjectionWithSetter implements InitializingBean {
	private final Logger log = LoggerFactory.getLogger(BeanInjectionWithSetter.class);

	@Autowired
	private ApplicationContext context;

	public void setContext(ApplicationContext context) {
		log.info("[{}] Dépendances injectées via la setter", this.getClass().getSimpleName());
		this.context = context;
	}

	public ApplicationContext getContext() {
		return context;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("[{}] Dépendances injectées ! (afterPropertiesSet)", this.getClass().getSimpleName());
		log.info("[{}] {}", this.getClass().getSimpleName(), this.getContext());
	}
}
