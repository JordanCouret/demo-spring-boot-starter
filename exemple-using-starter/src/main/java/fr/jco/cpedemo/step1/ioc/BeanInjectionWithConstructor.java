package fr.jco.cpedemo.step1.ioc;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class BeanInjectionWithConstructor {
	private final Logger log = LoggerFactory.getLogger(BeanInjectionWithConstructor.class);

	private final ApplicationContext context;

	public BeanInjectionWithConstructor(ApplicationContext context) {
		this.context = context;
		log.info("[{}] Dépendances injectées via la constructeur", this.getClass().getSimpleName());
		log.info("[{}] {}", this.getClass().getSimpleName(), this.getContext());
	}

	public ApplicationContext getContext() {
		return context;
	}
}
