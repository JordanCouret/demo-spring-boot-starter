package fr.jco.demo.starterrandombeans;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RandomAutoConfiguration {
	@ConditionalOnMissingBean
	@Bean
	public RandomGeneratorBean generatorBean() {
		return new RandomGeneratorBean();
	}
}
