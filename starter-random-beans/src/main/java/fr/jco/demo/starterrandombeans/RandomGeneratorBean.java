package fr.jco.demo.starterrandombeans;

import java.util.concurrent.ThreadLocalRandom;

public class RandomGeneratorBean {
	public int getInt() {
		return ThreadLocalRandom.current().nextInt();
	}
}
